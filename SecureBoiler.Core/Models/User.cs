﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace SecureBoiler.Core.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class User : IdentityUser
    {
        [Required]
        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string FirstName { get; set; }

        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string SecondName { get; set; }

        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string LastName { get; set; }
    }
}
