﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureBoiler.Core.Models
{
    public class Candidate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string FirstName { get; set; }

        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string SecondName { get; set; }

        [StringLength(maximumLength: 64, MinimumLength = 1)]
        public string LastName { get; set; }

        public string PhotoFileName { get; set; }

        public virtual Party Party { get; set; }
        public virtual ICollection<ElectionCandidates> ElectionCandidates { get; set; }
    }
}