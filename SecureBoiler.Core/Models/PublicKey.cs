﻿using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Models
{
    public struct PublicKey
    {
        public PublicKey(BigInteger modulus, BigInteger exponent)
        {
            this.Modulus = modulus.ToString();
            this.Exponent = exponent.ToString();
        }

        public static PublicKey CreateFrom(string modulus, string exponent)
        {
            return new PublicKey(new BigInteger(modulus), new BigInteger(exponent));
        }

        public string Modulus { get; }
        public string Exponent { get; }

        public RsaKeyParameters ConvertToKey()
        {
            return new RsaKeyParameters(false, new BigInteger(Modulus), new BigInteger(Exponent));
        }
    }
}
