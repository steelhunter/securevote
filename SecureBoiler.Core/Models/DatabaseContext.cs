﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using SecureBoiler.Core.Models;
using Microsoft.AspNet.Identity;

namespace SecureBoiler.Core.Models
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        private static bool _created = false;
        
        public DbSet<Election> Elections { get; set; }
        public DbSet<Voter> Voters { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<Party> Parties { get; set; }

        public DbSet<ElectionCandidates> ElectionCandidates { get; set; }

        public DatabaseContext()
        {
            if (!_created)
            {
                _created = true;
                
                Database.EnsureCreated();
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Add your customizations after calling base.OnModelCreating(builder);
            base.OnModelCreating(builder);
            
            #region ASP.NET Identity tables rename
            builder.Entity<User>()
                .ToTable(nameof(IdentityDbContext.Users));

            builder.Entity<IdentityRole>()
                .ToTable(nameof(IdentityDbContext.Roles));

            builder.Entity<IdentityUserRole<string>>()
                .ToTable(nameof(IdentityDbContext.UserRoles));

            builder.Entity<IdentityRoleClaim<string>>()
                .ToTable(nameof(IdentityDbContext.RoleClaims));

            builder.Entity<IdentityUserClaim<string>>()
                .ToTable(nameof(IdentityDbContext.UserClaims));

            builder.Entity<IdentityUserLogin<string>>()
                .ToTable(nameof(IdentityDbContext.UserLogins));
            #endregion

            builder.Entity<ElectionCandidates>()
                .HasKey(t => new { t.CandidateId, t.ElectionId }); // Candidate can appear in each poll only once

            // Many-to-many relationship between Polls and Candidates
            builder.Entity<ElectionCandidates>()
                .HasOne(t => t.Election)
                .WithMany(t => t.ElectionCandidates)
                .HasForeignKey(t => t.ElectionId);

            builder.Entity<ElectionCandidates>()
                .HasOne(t => t.Candidate)
                .WithMany(t => t.ElectionCandidates)
                .HasForeignKey(t => t.CandidateId);

            builder.Entity<Voter>()
                .HasOne(t => t.User);
            
            builder.Entity<Voter>()
                .ToTable(nameof(Voter) + "s");

            builder.Entity<Candidate>()
                .ToTable(nameof(Candidate) + "s");

            builder.Entity<Election>()
                .ToTable(nameof(Election) + "s")
                .HasMany(t => t.Voters);

            builder.Entity<Vote>()
                .ToTable(nameof(Vote) + "s");

            builder.Entity<Party>()
                .ToTable("Parties");
        }
    }
}
