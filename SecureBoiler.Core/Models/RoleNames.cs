﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Models
{
    public static class RoleNames
    {
        public const string Voter = "Voter";
        public const string Administrator = "Administrator";
    }
}
