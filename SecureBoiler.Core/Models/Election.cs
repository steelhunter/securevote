﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace SecureBoiler.Core.Models
{
    public class Election
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool IsOpen { get; set; }

        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Name { get; set; }

        [StringLength(maximumLength: 500, MinimumLength = 1)]
        public string Description { get; set; }

        public string CountryFlagFileName { get; set; }

        public virtual ICollection<ElectionCandidates> ElectionCandidates { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
        public virtual ICollection<Voter> Voters { get; set; }

        public Election()
        {
            ElectionCandidates = new List<ElectionCandidates>();
            Votes = new List<Vote>();
            Voters = new List<Voter>();
        }
    }
}
