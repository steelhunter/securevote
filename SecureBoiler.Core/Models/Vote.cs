﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Models
{
    public class Vote
    {
        [Key]
        public string Mark { get; set; }

        [Required]
        public byte[] MarkSignature { get; set; }

        [Required]
        public virtual Election Election { get; set; }

        [Required]
        public virtual Candidate Candidate { get; set; }
    }
}
