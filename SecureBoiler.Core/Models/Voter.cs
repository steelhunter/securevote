﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Models
{
    public class Voter
    {
        [Key]
        public byte[] BlindedMark { get; set; }
        
        public string PublicKeyModulus { get; set; }
        public string PublicKeyExponent { get; set; }


        public virtual User User { get; set; }

        [NotMapped]
        public PublicKey PublicKey {
            get {
                return PublicKey.CreateFrom(PublicKeyModulus, PublicKeyExponent);
            }
            set {
                this.PublicKeyExponent = value.Exponent;
                this.PublicKeyModulus = value.Modulus.ToString();
            }
        }
    }
}