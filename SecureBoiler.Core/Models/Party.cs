﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SecureBoiler.Core.Models
{
    public class Party
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 128, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [StringLength(maximumLength: 500, MinimumLength = 1)]
        public string Description { get; set; }
        
        public string LogoFileName { get; set; }

        public virtual ICollection<Candidate> Candidates { get; set; }
    }
}