﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace SecureBoiler.Core.Models
{
    public static class DatabaseInitializer
    {
        //public static void Truncate<T>(this DbSet<T> dbSet) where T : class
        //{
        //    dbSet.RemoveRange(dbSet);
        //}

        public async static void SeedDatabase(IServiceProvider serviceProvider, bool forceReseed = false)
        {
            var context = serviceProvider.GetService<DatabaseContext>();
            var userManager = serviceProvider.GetService<UserManager<User>>();
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (forceReseed)
            {
                // Crude database re-creation
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            //Roles
            if (!context.Roles.Any())
            {
                var roles = new List<IdentityRole>()
                {
                    new IdentityRole(RoleNames.Administrator),
                    new IdentityRole(RoleNames.Voter)
                };

                foreach (var role in roles)
                {
                    var task = roleManager.CreateAsync(role);
                    task.Wait();
                }
            }

            // Users
            if (!context.Users.Any())
            {
                var usersWithPasswords = new Dictionary<User, Tuple<string, string>>()
                {
                    {
                        new User { UserName = "admin@local.com", Email = "admin@local.com", FirstName = "Administrator" },
                        new Tuple<string, string>("AdminPassword", RoleNames.Administrator)
                    },
                    {
                        new User { UserName = "voter@local.com", Email = "voter@local.com",
                                   FirstName = "Mike", SecondName = "Kovalsky"},
                        new Tuple<string, string>("VoterPassword", RoleNames.Voter)
                    }
                };

                foreach (var pair in usersWithPasswords)
                {
                    await userManager.CreateAsync(pair.Key, pair.Value.Item1);
                    await userManager.AddToRoleAsync(pair.Key, pair.Value.Item2);
                }
            }

            // Parties
            if (!context.Parties.Any())
            {
                var parties = new List<Party>()
                {
                    new Party()
                    {
                        Name = "United Russia",
                        Description = "The current ruling political party in Russia, the largest party in the Russian Federation",
                        LogoFileName = "united_russia.png"
                    },
                    new Party()
                    {
                        Name = "LDPR",
                        Description = "The far-right political party in the Russian Federation. The charismatic and controversial Vladimir Zhirinovsky has led the party since its founding in 1989",
                        LogoFileName = "ldpr.jpg"
                    },
                    new Party()
                    {
                        Name = "Republican Party (GOP)",
                        Description = "One of the two major contemporary political parties in the United States. Founded by anti-slavery activists, modernists, ex-Whigs, and ex-Free Soilers in 1854, the Republicans dominated politics nationally and in the majority of northern States for most of the period between 1860 and 1932",
                        LogoFileName = "republicans.jpg"
                    },
                    new Party()
                    {
                        Name = "Democratic Party",
                        Description = "One of the two major contemporary political parties in the United States. Tracing its heritage back to Thomas Jefferson's and James Madison's Democratic-Republican Party, the modern-day Democratic Party was founded around 1828, making it the world's oldest active party",
                        LogoFileName = "democrats.png"
                    }
                };

                context.Parties.AddRange(parties);
                context.SaveChanges();
            }

            // Candidates
            if (!context.Candidates.Any())
            {
                var candidates = new List<Candidate>()
                {
                    new Candidate()
                    {
                        FirstName = "Donald",
                        SecondName = "John",
                        LastName = "Trump",
                        Party = context.Parties.Single(party => party.Name == "Republican Party (GOP)"),
                        PhotoFileName = "trump.jpg"
                    },
                    new Candidate()
                    {
                        FirstName = "Hillary",
                        SecondName = "Rodham",
                        LastName = "Clinton",
                        Party = context.Parties.Single(party => party.Name == "Democratic Party"),
                        PhotoFileName = "hillary_clinton.jpg"
                    },
                    new Candidate()
                    {
                        FirstName = "Bernard",
                        SecondName = "Sanders",
                        Party = context.Parties.Single(party => party.Name == "Democratic Party"),
                        PhotoFileName = "sanders.jpg"
                    },
                    new Candidate()
                    {
                        FirstName = "Vladimir",
                        SecondName = "Putin",
                        LastName = "Vladimirovich",
                        Party = context.Parties.Single(party => party.Name == "United Russia"),
                        PhotoFileName = "putin.jpg"
                    },
                    new Candidate()
                    {
                        FirstName = "Vladimir",
                        SecondName = "Zhirinovsky",
                        LastName = "Volfovich",
                        Party = context.Parties.Single(party => party.Name == "LDPR"),
                        PhotoFileName = "zhirinovsky.jpg"
                    }
                };

                context.Candidates.AddRange(candidates);
                context.SaveChanges();
            }

            // Elections
            if (!context.Elections.Any())
            {
                var elections = new List<Election>()
                {
                    new Election() {
                        Name = "2016 American Presidential Election",
                        Description = "Scheduled for Tuesday, November 8, 2016, will be the 58th quadrennial U.S. presidential election",
                        IsOpen = true,
                        CountryFlagFileName = "usa.png"
                    },
                    new Election() {
                        Name = "2018 Russian Presidential Election",
                        Description = "The first round will be held March 11, 2018",
                        IsOpen = false,
                        CountryFlagFileName = "russia.jpg"
                    }
                };

                context.Elections.AddRange(elections);
                context.SaveChanges();
            }

            // ElectionCandidates
            // Many-to-many relationship hack for EF7
            if (!context.ElectionCandidates.Any())
            {
                var election2016 = context.Elections.Single(election => election.Name == "2016 American Presidential Election");
                var election2018 = context.Elections.Single(election => election.Name == "2018 Russian Presidential Election");

                var trump = context.Candidates.Single(c => c.LastName == "Trump");
                var sanders = context.Candidates.Single(c => c.SecondName == "Sanders");
                var clinton = context.Candidates.Single(c => c.LastName == "Clinton");

                var putin = context.Candidates.Single(c => c.SecondName == "Putin");
                var zhirinovsky = context.Candidates.Single(c => c.SecondName == "Zhirinovsky");

                var electionCandidates = new List<ElectionCandidates>()
                {
                    new ElectionCandidates() { Election = election2016, Candidate = trump },
                    new ElectionCandidates() { Election = election2016, Candidate = sanders },
                    new ElectionCandidates() { Election = election2016, Candidate = clinton },

                    new ElectionCandidates() { Election = election2018, Candidate = putin },
                    new ElectionCandidates() { Election = election2018, Candidate = zhirinovsky }
                };

                context.ElectionCandidates.AddRange(electionCandidates);
                context.SaveChanges();
            }

            // Voters
            if (!context.Voters.Any())
            {

            }
            
            // Votes
            if (!context.Votes.Any())
            {

            }
        }
    }
}
