﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Models
{
    public class ElectionCandidates
    {
        public int ElectionId { get; set; }
        public virtual Election Election { get; set; }

        public int CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
    }
}
