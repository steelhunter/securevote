﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Helpers
{
    public class Result
    {
        public bool Success { get; }
        public bool Failure => !Success;

        protected Result(bool success)
        {
            Success = success;
        }

        public static Result Ok()
        {
            return new Result(true);
        }

        public static Result Error()
        {
            return new Result(false);
        }

        public static Result<T> Ok<T>(T value)
            where T : class
        {
            return new Result<T>(value, true);
        }

        public static Result<T> Error<T>()
            where T : class
        {
            return new Result<T>(default(T), false);
        }
    }
}
