﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Core.Helpers
{
    public class Result<T> : Result
        where T : class
    {
        private readonly T _value;

        public T Value
        {
            get
            {
                if (Failure)
                    throw new InvalidOperationException();

                return _value;
            }
        }

        protected internal Result(T value, bool success) 
            : base(success)
        {
            var valid = (value != default(T) && success) ||
                        (value == default(T) && !success);

            if (!valid)
                throw new InvalidOperationException();

            _value = value;
        }

        public static implicit operator T(Result<T> result)
        {
            if (result.Failure)
                throw new InvalidOperationException();

            return result.Value;
        }
    }
}
