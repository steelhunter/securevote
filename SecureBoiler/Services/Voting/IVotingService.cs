﻿using SecureBoiler.Core.Helpers;
using SecureBoiler.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Services
{
    public interface IVotingService
    {
        Result<byte[]> GetBlindSignatureFor(User user);
        Result VerifySignatureOn(User user);
        Result RecordNewVote(Vote vote);
        PublicKey GetCommitteeKey();
        List<Candidate> GetCandidatesFor(Election poll);
    }
}
