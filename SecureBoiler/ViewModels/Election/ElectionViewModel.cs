﻿using SecureBoiler.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.ViewModels.Election
{
    public class ElectionViewModel
    {
        public int Id { get; set; }
        public bool IsOpen { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CountryFlagFileName { get; set; }

        public IList<Candidate> Candidates { get; set; }

        public ElectionViewModel()
        {
            Candidates = new List<Candidate>();
        }

    }
}
