﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.ViewModels.Election
{
    public class ElectionListViewModel
    {
        public ElectionListViewModel()
        {
            Elections = new List<Core.Models.Election>();
        }

        public List<Core.Models.Election> Elections { get; set; }
    }
}
