﻿using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Html.Abstractions;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Mvc.ViewFeatures;
using Microsoft.AspNet.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SecureBoiler.Framework.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlContent Image(this IHtmlHelper helper, string src, string altText, object htmlAttributes = null)
        {
            var img = new TagBuilder("img");
            img.TagRenderMode = TagRenderMode.SelfClosing;

            img.MergeAttribute("src", src);
            img.MergeAttribute("alt", altText);

            if (htmlAttributes != null)
                img.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            
            return img;
        }

        public static IHtmlContent FlagImage(this IHtmlHelper helper, IUrlHelper urlHelper, string flagName, string margins = "", int width = 200, int height = 150)
        {
            var src = urlHelper.Content($"~/img/flags/{ WebUtility.UrlEncode(flagName) }");
            var alt = "Country flag";

            var attributes = new { style = $"width: {width}px; height: {height}px;" + margins };

            return Image(helper, src, alt, attributes);
        }

        public static IHtmlContent CandidateImage(this IHtmlHelper helper, IUrlHelper urlHelper, string photoFileName, string margins = "", int width = 200, int height = 150)
        {
            var src = urlHelper.Content($"~/img/candidates/{ WebUtility.UrlEncode(photoFileName) }");
            var alt = "Candidate photo";

            var attributes = new { style = $"width: {width}px; height: {height}px;" + margins };

            return Image(helper, src, alt, attributes);
        }
    }
}
