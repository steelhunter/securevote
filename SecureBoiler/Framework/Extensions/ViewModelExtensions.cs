﻿using SecureBoiler.ViewModels.Election;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SecureBoiler.Core.Models;

namespace SecureBoiler.Framework.Extensions
{
    public static class ViewModelExtensions
    {
        public static ElectionViewModel ToViewModel(this Election election, IMapper mapper, IEnumerable<Candidate> candidates)
        {
            
            if (election == null)
                return null;
            
            var baseModel = mapper.Map<Election, ElectionViewModel>(election);

            if (candidates != null)
                baseModel.Candidates = new List<Candidate>(candidates);

            return baseModel;
        }
    }
}
