﻿using AutoMapper;
using SecureBoiler.Core.Models;
using SecureBoiler.ViewModels.Election;
using System.Linq.Expressions;

namespace SecureBoiler.Framework
{
    public class MappingConfiguration : Profile
    {
        protected override void Configure()
        {
            AllowNullCollections = true;

            CreateMap<Election, ElectionViewModel>()
                .ForMember(dest => dest.Candidates, i => i.Ignore());
        }
    }
}
