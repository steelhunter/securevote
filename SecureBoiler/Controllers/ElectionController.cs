﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using SecureBoiler.Core.Models;
using SecureBoiler.Core.Helpers;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using SecureBoiler.ViewModels.Election;
using SecureBoiler.Framework.Extensions;
using SecureBoiler.Constants;
using AutoMapper;
using Microsoft.Data.Entity;

namespace SecureBoiler.Controllers
{
    [Authorize]
    [Route("elections")]
    public class ElectionController : Controller
    {
        private readonly DatabaseContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly IMapper _mapper;

        public ElectionController(DatabaseContext context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IMapper mapper)
        {
            this._context = context;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._mapper = mapper;
        }
        
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index()
        {
            var viewModel = new ElectionListViewModel();
            var userResult = await GetCurrentUserAsync();
            
            if (userResult.Success)
            {
                var elections = _context.Elections.ToList();

                if (elections != null)
                    viewModel.Elections = elections;
            }

            return View(viewModel);
        }

        [HttpGet]
        [Route("{electionId:int}")]
        public IActionResult Details(int electionId)
        {
            var election = _context.Elections.SingleOrDefault(e => e.Id == electionId);

            if (election == null)
                return RedirectToAction(nameof(Index));

            _context.Parties.Load();

            var candidates = _context.ElectionCandidates
                .Where(ec => ec.Election.Id == election.Id)
                .Select(el => el.Candidate)
                .Include(c => c.Party);

            return View(election.ToViewModel(_mapper, candidates));
        }

        private async Task<IList<string>> GetUserRolesAsync(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);

            return roles;
        }
        private async Task<Result<User>> GetCurrentUserAsync()
        {
            var user = await _userManager.FindByIdAsync(HttpContext.User.GetUserId());

            if (user == null)
                return Result.Error<User>();

            return Result.Ok(user);
        }
    }
}
