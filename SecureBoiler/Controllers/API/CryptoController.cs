﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using SecureBoiler.Services;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Crypto.Signers;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Digests;

namespace SecureBoiler.Controllers.API
{
    [Route("api/[controller]")]
    public class CryptoController : Controller
    {
        private readonly IVotingService _votingService;

        public CryptoController(IVotingService votingService)
        {
            this._votingService = votingService;
        }

        // GET api/crypto/blind
        [HttpGet("blinded/{mark}")]
        public string Get(string mark)
        {
            var integerMark = new BigInteger(mark);
            var markBytes = integerMark.ToByteArray();

            var committeeKeyModel = _votingService.GetCommitteeKey();
            var rsaKey = committeeKeyModel.ConvertToKey();

            // Blind a mark
            var blindingFactorGenerator = new RsaBlindingFactorGenerator();
            blindingFactorGenerator.Init(rsaKey);

            var blindingFactor = blindingFactorGenerator.GenerateBlindingFactor();
            var blindingParameters = new RsaBlindingParameters(rsaKey, blindingFactor);

            // Perform actual signing
            var pssSigner = new PssSigner(new RsaBlindingEngine(), new Sha512Digest());
            pssSigner.Init(true, blindingParameters);
            pssSigner.BlockUpdate(markBytes, 0, markBytes.Length);

            var blindedMark = pssSigner.GenerateSignature();

            return Convert.ToBase64String(blindedMark);
        }

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
