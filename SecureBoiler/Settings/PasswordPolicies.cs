﻿using Microsoft.AspNet.Identity;

namespace SecureBoiler.Settings
{
    
    public class PasswordPolicies
    {
        public static PasswordOptions Default => new PasswordOptions();

        /// <summary>
        /// Accepts passwords length of 6 or more
        /// </summary>
        public static PasswordOptions Relaxed =>
            new PasswordOptions()
            {
                RequireUppercase = false,
                RequireDigit = false,
                RequireNonLetterOrDigit = false,
                RequiredLength = 6
            };
    }
}
