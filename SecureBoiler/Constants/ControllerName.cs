﻿using SecureBoiler.Controllers;
using System;
using System.Diagnostics;

namespace SecureBoiler.Constants
{
    public static class ControllerName
    {
        public const string Error = nameof(Error);
        public const string Home = nameof(Home);
        public const string Account = nameof(Account);
        public const string Election = nameof(Election);
    }
}