namespace SecureBoiler.Constants
{
    public static class HomeControllerRoute
    {
        public const string GetAbout = ControllerName.Home + "GetAbout";
        public const string GetBrowserConfigXml = ControllerName.Home + "GetBrowserConfigXml";
        public const string GetIndex = ControllerName.Home + "GetIndex";
        public const string GetManifestJson = ControllerName.Home + "GetManifestJson";
        public const string GetRobotsText = ControllerName.Home + "GetRobotsText";
    }
}
