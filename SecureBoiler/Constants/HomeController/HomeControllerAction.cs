namespace SecureBoiler.Constants
{
    public static class HomeControllerAction
    {
        public const string About = "About";
        public const string Index = "Index";
        public const string RobotsText = "RobotsText";
    }
}
