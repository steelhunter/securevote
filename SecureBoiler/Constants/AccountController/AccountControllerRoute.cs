﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureBoiler.Constants
{
    public static class AccountControllerRoute
    {
        public const string GetLogin = ControllerName.Account + "Get" + AccountControllerAction.Login;
        public const string PostLogin = ControllerName.Account + "Post" + AccountControllerAction.Login;

        public const string GetRegister = ControllerName.Account + "Get" + AccountControllerAction.Register;
        public const string PostRegister = ControllerName.Account + "Post" + AccountControllerAction.Register;

        public const string PostLogOff = ControllerName.Account + "Post" + AccountControllerAction.LogOff;

        
    }
}
