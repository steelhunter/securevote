﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SecureBoiler.Controllers;

namespace SecureBoiler.Constants
{
    public static class AccountControllerAction
    {
        public const string Register = nameof(AccountController.Register);
        public const string Login = nameof(AccountController.Login);
        public const string LogOff = nameof(AccountController.LogOff);
    }
}
