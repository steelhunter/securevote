﻿namespace SecureBoiler
{
    using Core.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Data.Entity;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Settings;

    public partial class Startup
    {
        private static void ConfigureDatabaseWithIdentity(IServiceCollection services, IConfiguration configuration)
        {
            // Add framework services.
            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<DatabaseContext>(options =>
                    options.UseSqlServer(configuration["Data:DbConnection:ConnectionString"]));

            // Identity provider
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders();

            // Configure password policy
            services.Configure<IdentityOptions>(options => 
                options.Password = PasswordPolicies.Relaxed);
        }
    }
}
