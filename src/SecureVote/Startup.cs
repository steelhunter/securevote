﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SecureVote.Models;
using SecureVote.Services;
using System.Globalization;
using Microsoft.AspNet.Localization;
using SecureVote.Helpers;

namespace SecureVote
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<Models.DatabaseContext>(options =>
                    options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<Models.DatabaseContext>()
                .AddDefaultTokenProviders();

            // Localization services
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                // Find localized views using suffix: Index.ru-RU.cshtml
                .AddViewLocalization(Microsoft.AspNet.Mvc.Razor.LanguageViewLocationExpanderFormat.Suffix)
                // Data annotations localization via IStringLocalizer 
                .AddDataAnnotationsLocalization();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            // TODO: will be enabled in RC2, refactor from IApplicationBuilder
            // Configure supported cultures
            //services.Configure<RequestLocalizationOptions>(options =>
            //{
            //    var supportedCultures = new[]
            //    {
            //        Cultures.English,
            //        Cultures.Russian
            //    };

            //    // Default culture for request w/o determined culture
                
            //    //options.DefaultRequestCulture = new RequestCulture(new CultureInfo("en-US"));

            //    // Supported cultures for framework classes: numbers, dates, etc.
            //    options.SupportedCultures = supportedCultures;

            //    // Supported cultures that we have translated UI strings for
            //    options.SupportedUICultures = supportedCultures;

            //    // You can change which providers are configured to determine the culture for requests, or even add a custom
            //    // provider with your own logic. The providers will be asked in order to provide a culture for each request,
            //    // and the first to provide a non-null result that is in the configured supported cultures list will be used.
            //    // By default, the following built-in providers are configured:
            //    // - QueryStringRequestCultureProvider, sets culture via "culture" and "ui-culture" query string values, useful for testing
            //    // - CookieRequestCultureProvider, sets culture via "ASPNET_CULTURE" cookie
            //    // - AcceptLanguageHeaderRequestCultureProvider, sets culture via the "Accept-Language" request header
            //    //options.RequestCultureProviders.Insert(0, new CustomRequestCultureProvider(async context =>
            //    //{
            //    //  // My custom request culture logic
            //    //  return new ProviderCultureResult("en");
            //    //}));
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                // For more details on creating database during deployment see http://go.microsoft.com/fwlink/?LinkID=615859
                try
                {
                    using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                        .CreateScope())
                    {
                        serviceScope.ServiceProvider.GetService<Models.DatabaseContext>()
                             .Database.Migrate();
                    }
                }
                catch { }
            }

            app.UseIISPlatformHandler(options => options.AuthenticationDescriptions.Clear());

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseIdentity();

            // To configure external authentication please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Temporary localization until RC2
            var supportedCultures = new[]
            {
                Cultures.English,
                Cultures.Russian
            };

            // Default culture for request w/o determined culture
            var defaultRequestCulture = new RequestCulture(Cultures.English);

            var localizationOptions = new RequestLocalizationOptions()
            {
                 //Supported cultures for framework classes: numbers, dates, etc.
                 SupportedCultures = supportedCultures,

                 // Supported cultures that we have translated UI strings for
                 SupportedUICultures = supportedCultures
            };

            app.UseRequestLocalization(localizationOptions, defaultRequestCulture);
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
