﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace SecureVote.Models
{
    public class PublicKey
    {
        public BigInteger Modulus;
        public int Exponent;
    }
}
