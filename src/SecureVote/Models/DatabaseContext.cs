﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using SecureVote.Models;

namespace SecureVote.Models
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        private static bool _created = false;

        public DatabaseContext()
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureCreated();
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<PollCandidates>()
                .HasKey(t => new { t.CandidateId, t.PollId }); // Candidate can appear in each poll only once

            // Many-to-many relationship between Polls and Candidates
            builder.Entity<PollCandidates>()
                .HasOne(t => t.Poll)
                .WithMany(t => t.PollCandidates)
                .HasForeignKey(t => t.PollId);

            builder.Entity<PollCandidates>()
                .HasOne(t => t.Candidate)
                .WithMany(t => t.PollCandidates)
                .HasForeignKey(t => t.CandidateId);

        }

        public DbSet<Poll> Polls { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Party> Parties { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<User> User { get; set; }
    }
}
