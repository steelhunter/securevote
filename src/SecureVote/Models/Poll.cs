﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace SecureVote.Models
{
    public class Poll
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Name { get; set; }

        [StringLength(maximumLength: 255, MinimumLength = 1)]
        public string Description { get; set; }

        public virtual ICollection<PollCandidates> PollCandidates { get; set; }
        public virtual ICollection<Vote> Votes { get; }
    }
}
