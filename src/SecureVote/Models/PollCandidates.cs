﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.Models
{
    public class PollCandidates
    {
        public int PollId { get; set; }
        public virtual Poll Poll { get; set; }

        public int CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
    }
}
