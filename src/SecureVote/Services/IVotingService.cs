﻿using SecureVote.Helpers;
using SecureVote.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.Services
{
    interface IVotingService
    {
        Result<byte[]> GetBlindSignatureFor(User user);
        Result VerifySignatureOn(User user);
        Result RecordNewVote(Vote vote);
        PublicKey GetCommitteeKey();
        List<Candidate> GetCandidatesFor(Poll poll);

    }
}
