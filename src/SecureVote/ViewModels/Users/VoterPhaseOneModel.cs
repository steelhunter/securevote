﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.ViewModels.Users
{
    public class VoterPhaseOneModel
    {
        public string BlindedMark { get; set; }
    }
}
