﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.ViewModels.Users
{
    public class PhaseTwoViewModel
    {
        public string BlindedSignature { get; set; }
    }
}
