﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.Helpers
{
    /// <summary>
    /// Represents various cultures throughout the app
    /// </summary>
    public class Cultures
    {
        /// <summary>
        /// English (United States)
        /// </summary>
        public static CultureInfo English => new CultureInfo("en-US");

        /// <summary>
        /// Russian (Russia)
        /// </summary>
        public static CultureInfo Russian => new CultureInfo("ru-RU");

        /// <summary>
        /// Culture-agnostic
        /// </summary>
        public static CultureInfo Invariant => CultureInfo.InvariantCulture;
    }
}
