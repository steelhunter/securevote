﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.Helpers
{
    public class Result<T> : Result
        where T : class
    {
        public T Value
        {
            get
            {
                if (Failure)
                    throw new InvalidOperationException();

                return Value;
            }
            private set
            {
                this.Value = value;
            }
        }

        protected Result(T value, bool success) 
            : base(success)
        {
            this.Value = value;
        }

        public Result<T> Ok(T value)
        {
            return new Result<T>(value, true);
        }

        public Result<T> Error(T value)
        {
            return new Result<T>(default(T), false);
        }
    }
}
