﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureVote.Helpers
{
    public class Result
    {
        public bool Success { get; }
        public bool Failure => !Success;

        protected Result(bool success)
        {
            Success = success;
        }

        public Result Ok()
        {
            return new Result(true);
        }

        public Result Error()
        {
            return new Result(false);
        }
    }
}
